# ./spark-submit /dirname
# submit python script to spark

from __future__ import print_function
from pyspark import SparkContext
sc = SparkContext("local","simple app")

#a=[1,4,3,5]
#b = sc.parallelize(a)
#print b
#print b.take(2)

## map: one old RDD create one new RDD
#x = sc.parallelize(a)
#y = x.map(lambda x: (x, x**2))
#print x.collect()
#print y.collect()

## flatmap: can return multiple new RDD for one old RDD
#x = sc.parallelize(a)
#y = x.flatMap(lambda x: (x, 100*x, x**2))
#print x.collect()
#print y.collect()

## mapPartitions: Do map function on different partition
#x = sc.parallelize(a, 2)
#def f(iterator): yield sum(iterator)
#y = x.mapPartitions(f)
#print x.glom().collect()
#print y.glom().collect()

## mapPartitionsWIthIndex
#x = sc.parallelize(a, 2)
#def f(partitionIndex, iterator): yield (partitionIndex, sum(iterator))
#y = x.mapPartitionsWithIndex(f)
#print x.glom().collect()
#print y.glom().collect()
#print x.getNumPartitions()

# filter
#x = sc.parallelize([1, 2, 3])
#y = x.filter(lambda x: x%2 == 1)
#print x.collect()
#print y.collect()

# Distinct
#x = sc.parallelize([1, 1, 2, 2, 3, 3, 4, 4, 5, 5])
#y = x.distinct()
#print x.collect()
#print y.collect()

# sample
#x = sc.parallelize(range(7))
#ylist = [x.sample(True, 1) for i in range(5)]
#print 'x = ' + str(x.collect())
#for cnt, y in zip(range(len(ylist)), ylist):
#	print 'sample: ' + str(cnt) + ' y = ' + str(y.collect())

# takeSample
#x = sc.parallelize(range(7))
#ylist = [x.takeSample(False, 3) for i in range(7)]
#print 'x = ' + str(x.collect())
#for cnt, y in enumerate(ylist):
#	print 'sample: ' + str(cnt) + ' y = ' + str(y)

# union
#x = sc.parallelize(['A', 'A', 'B'])
#y = sc.parallelize(['D', 'C', 'R'])
#z = x.union(y)
#print x.collect()
#print y.collect()
#print z.collect()

#x = sc.parallelize([1,2,3])
#def f(el):
#    '''side effect: append the current RDD elements to a file'''
#    f1=open("./test.txt", 'a+')
#    print(el,file=f1)

#open('./test.txt', 'w').close()  # first clear the file contents

#y = x.foreach(f) # writes into foreachExample.txt

#print(x.collect())
#print(y) # foreach returns 'None'
## print the contents of foreachExample.txt
#with open("./test.txt", "r") as foreachExample:
#    print (foreachExample.read())

# intersection
#x = sc.parallelize(['A', 'A', 'B'])
#y = sc.parallelize(['A', 'C', 'D'])
#z = x.intersection(y)
#print (x.collect())
#print(y.collect())
#print(z.collect())

# sortByKey
#x = sc.parallelize([('B', 1), ('A', 2), ('C', 3)])
#y = x.sortByKey()
#print(x.collect())
#print(y.collect())

# sortBy
#x = sc.parallelize(['Cat', 'Apple', 'Bat'])
#y = x.sortBy(lambda x: x[1])
#print(y.collect())

# cartesian
x = sc.parallelize(['A', 'B'])
y = sc.parallelize(['c', 'd'])
z = x.cartesian(y)
print(z.collect())

# groupBy
#rdd = sc.parallelize([1, 1, 2, 3, 5, 8])
#res = rdd.groupBy(lambda x: x%2).collect()
#print(sorted([(x, sorted(y)) for (x, y) in res]))

# foreachPartition
#x = sc.parallelize([1, 2, 3])
#def f(partition):
#	f1 = open('./foreachPartitionExample.txt', 'a+')
#	print([el for el in partition], file = f1)

#open('./foreachPartitionExample.txt', 'w').close()
#y = x.foreachPartition(f)

#print(x.glom().collect())
#print(y)
#with open('./foreachPartitionExample.txt', 'r') as foreachExample:
#	print(foreachExample.read())

# reduce
#x = sc.parallelize([1, 2, 3])
#y = x.reduce(lambda obj, accumulated: obj + accumulated)
#print(x.collect())
#print(y)

# fold
#x = sc.parallelize([1, 2, 3, 4, 5, 6], 2)
#neutralZeroValue = 0
#y = x.fold(neutralZeroValue, lambda obj, accumulated: accumulated + obj)
#print(x.collect())
#print(y)

# aggregate
#x = sc.parallelize([2, 3, 4])
#neutralZeroValue = (0, 1)
#seqOp = (lambda aggregated, el: (aggregated[0] + el, aggregated[1]*el))
#combOp = (lambda aggregated, el: (aggregated[0] + el[0], aggregated[1]*el[1]))
#y = x.aggregate(neutralZeroValue, seqOp, combOp)
#print(x.collect())

# countByValue
#x = sc.parallelize([1, 3, 1, 2, 3])
#y = x.countByValue()
#print(x.collect())
#print(y)

# top/takeOrdered/first
#x = sc.parallelize(range(10))
#y = x.first()
#print(y)

# histogram
#x = sc.parallelize([1, 3, 1, 2, 3])
#y = x.histogram([0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5])
#print(y)

# collectAsMap, give KV return dictionary
#x = sc.parallelize([('c', 3), ('a', 1), ('b', 2)])
#y = x.collectAsMap()
#print(y)

# KV: keys: return keys, values: return values

# ReduceByKey
#x = sc.parallelize([('b', 1), ('b', 2), ('a', 3), ('a', 4), ('a', 5)])
#y = x.reduceByKey(lambda agg, obj: agg + obj)
#print(x.collect())
#print(y.collect())

# COUNTBYKEY
#x = sc.parallelize([('b', 1), ('b', 2), ('a', 3), ('a', 4), ('a', 5)])
#y = x.countByKey()
#print(y)

# JOIN return all possible combination
#x = sc.parallelize([('a', 1), ('b', 4)])
#y = sc.parallelize([('a', 2), ('a', 3)])
#print(sorted(x.join(y).collect()))

# partitionBy
# combineByKey
#create a combiner: to be used as the very first aggregation step for each keys
#merge a value: what to do when a combiner is given a new values
#merge two combiners: how to merge two combiners
#x = sc.parallelize([('b', 1), ('b', 2), ('a', 3), ('a', 4), ('a', 5), ('c', 5)])
#createCombiner = (lambda el: (el, el**3))
#mergeVal = (lambda aggregated, el: (aggregated[0] + el, aggregated[1] + el))
#mergeComb = (lambda agg1, agg2: agg1 + agg2)
#y = x.combineByKey(createCombiner, mergeVal, mergeComb)
#print(y.collect())

# aggregateByKey: replace createCombiner as a initial values

# foldByKey
#x = sc.parallelize([('b', 1), ('b', 2), ('a', 3), ('a', 4)])
#zeroValue = 1
#y = x.foldByKey(zeroValue, lambda agg, x: agg*x)
#print(y.collect())

# partitionBy: old partition to new partition
#x = sc.parallelize([(0, 1), (1, 2), (2, 3)], 2)
#y = x.partitionBy(3, lambda x: x)
#print(x.glom().collect())
#print(y.glom().collect())

# flatMapValues
#x = sc.parallelize([('a', (1, 2, 3)), ('b', (4, 5))])
#y = x.flatMapValues(lambda x: [i**2 for i in x])
#print(y.collect())

# groupWith
#x = sc.parallelize([('C',4),('B',(3,3)),('A',2),('A',(1,1))])
#y = sc.parallelize([('B',(7,7)),('A',6),('D',(5,5))])
#z = sc.parallelize([('D',9),('B',(8,8))])
#a = x.groupWith(y,z)
#print(x.collect())
#print(y.collect())
#print(z.collect())
#print("Result:")
#for key,val in list(a.collect()): 
#	print(key, [list(i) for i in val])


