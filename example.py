#from __future__ import print_function
import urllib 
from pyspark import SparkContext
sc = SparkContext("local","simple app")

#f = urllib.urlretrieve ("http://kdd.ics.uci.edu/databases/kddcup99/kddcup.data_10_percent.gz", "kddcup.data_10_percent.gz")
data_file = './kddcup.data_10_percent.gz'
raw_data = sc.textFile(data_file)
print raw_data.count()
print raw_data.take(5)

normal_raw_data = raw_data.filter(lambda x: 'normal.' in x)
from time import time
t0 = time()
normal_count = normal_raw_data.count()
tt = time() - t0
print "there are {}'normal' interactions".format(normal_count)
print "count complete in {} seconds".format(round(tt, 3))

from pprint import pprint
csv_data = raw_data.map(lambda x: x.split(','))
t0 = time()
head_rows = csv_data.take(5)
tt = time() - t0
print "parse complete in {} seconds".format(round(tt, 3))
pprint(head_rows[0])

def parse_interaction(line):
	elems = line.split(',')
	tag = elems[41]
	return (tag, elems)

key_csv_data = raw_data.map(parse_interaction)
head_rows = key_csv_data.take(5)
pprint(head_rows[0])
normal_key_interactions = key_csv_data.filter(lambda x: x[0] == 'normal.')

t0 = time()
all_raw_data = raw_data.collect()
tt = time() - t0
print 'Data collected in {} seconds'.format(round(tt, 3))

raw_data_sample = raw_data.sample(False, 0.1, 1234)
sample_size = raw_data_sample.count()
total_size = raw_data.count()
print 'sample size is {} of {}'.format(sample_size, total_size)

t0 = time()
raw_data_sample = raw_data.takeSample(False, 400000, 1234)
normal_data_sample = [x.split(',') for x in raw_data_sample if 'normal.' in x]
tt = time() - t0
normal_sample_size = len(normal_data_sample)
normal_ratio = normal_sample_size/400000.0
print 'the ratio of "normal" interactions is {}'.format(normal_ratio)
print 'count done in {} seconds'.format(round(tt, 3))

attack_raw_data = raw_data.subtract(normal_raw_data)

protocols = csv_data.map(lambda x: x[1]).distinct()
print protocols.collect()
services = csv_data.map(lambda x: x[2]).distinct()
print services.collect()
product = protocols.cartesian(services).collect()
print len(product)

normal_csv_data = normal_raw_data.map(lambda x: x.split(','))
attack_csv_data = attack_raw_data.map(lambda x: x.split(','))
normal_duration_data = normal_csv_data.map(lambda x: int(x[0]))
attack_duration_data = attack_csv_data.map(lambda x: int(x[0]))
total_normal_duration = normal_duration_data.reduce(lambda x, y: x + y)
total_attack_duration = attack_duration_data.reduce(lambda x, y: x + y)
print 'total duration for "normal" interactions is {}'.format(total_normal_duration)
print 'total duration for "attack" interactions is {}'.format(total_attack_duration)

normal_sum_count = normal_duration_data.aggregate(
	(0, 0),
	(lambda acc, value: (acc[0] + value, acc[1] + 1)),
	(lambda acc1, acc2: (acc1[0] + acc2[0], acc1[1] + acc2[1]))
)
print "mean duration for 'normal' interactions is {}".format(round(normal_sum_count[1]/float(normal_sum_count[1]), 3))


